//
// Created by chencaikun on 2023/8/23.
//

#ifndef WEBPROJECT_BLOCK_QUEUE_H
#define WEBPROJECT_BLOCK_QUEUE_H

#include<iostream>
#include<pthread.h>
#include<sys/time.h>
#include "../lock/locker.h"

using namespace std;

template <class T>
class block_queue
{
public:
    /**
     * @brief 构造函数，初始化阻塞队列
     * @param max_size 队列的最多存储元素个数，默认1000
     */
    block_queue(int max_size = 1000)
    {
        if(max_size <= 0)
        {
            exit(-1);
        }
        m_max_size = max_size;
        m_array = new T[m_max_size];
        m_front = -1;
        m_back = -1;
        m_size = 0;
    }
    /**
     * @brief 析构函数，释放new申请的内存
     */
    ~block_queue()
    {
        m_mutex.lock();
        if(m_array != NULL)
        {
            delete [] m_array;
        }
        m_mutex.unlock();
    }
    /**
     * @brief 清空阻塞队列
     */
    void clear()
    {
        m_mutex.lock();
        m_size = 0;
        m_front = -1;
        m_back = -1;
        m_mutex.unlock();
    }
    /**
     * @brief 判断阻塞队列是否满
     * @return
     */
    bool full()
    {
        m_mutex.lock();
        if(m_size >= m_max_size)
        {
            m_mutex.unlock();
            return true;
        }
        else
        {
            m_mutex.unlock();
            return false;
        }
    }
    /**
     * @brief 判断阻塞队列是否为空
     * @return
     */
    bool empty()
    {
        m_mutex.lock();
        if(m_size ==0)
        {
            m_mutex.unlock();
            return true;
        }
        else
        {
            m_mutex.unlock();
            return false;
        }
    }
    /**
     * @brief 获取阻塞队列的首元素
     * @param value 阻塞队列的元素类型的引用
     * @return 是否获取成功
     */
    bool front(T &value)
    {
        m_mutex.lock();
        if(m_size == 0)
        {
            m_mutex.unlock();
            return false;
        }
        value = m_array[m_front];
        m_mutex.unlock();
        return true;
    }
    /**
     * @brief 获取阻塞队列的尾元素
     * @param value 元素类型的引用
     * @return 是否获取成功
     */
    bool back(T &value)
    {
        m_mutex.lock();
        if(m_size == 0)
        {
            m_mutex.unlock();
            return false;
        }
        value = m_array[m_back];
        m_mutex.unlock();
        return true;
    }
    /**
     * @brief 返回阻塞队列的大小
     * @return
     */
    int size()
    {
        m_mutex.lock();
        int num = m_size;
        m_mutex.unlock();
        return num;
    }
    /**
     * @brief 返回阻塞队列的最大值
     * @return
     */
    int max_size()
    {
        m_mutex.lock();
        int num = m_max_size;
        m_mutex.unlock();
        return num;
    }
    /**
     * @brief 向阻塞队列的尾部添加一个元素，相当于生产者生产一个元素，
     * 之后唤醒所有等待队列的线程，若没有等待该条件变量的线程，唤醒将毫无意义
     * @param item 添加到阻塞队列的元素
     * @return
     */
    bool push(const T &item)
    {
        m_mutex.lock();
        if(m_size >= m_max_size)
        {
            m_mutex.unlock();
            return false;
        }
        m_back = (m_back + 1) % m_max_size;
        m_array[m_back] = item;
        m_size++;

        m_cond.broadcast();
        m_mutex.unlock();
        return true;
    }
    /**
     * @brief 阻塞队列中弹出一个元素，如果没有时将会等待条件变量
     * @param item 存储获得的元素
     * @return 是否获取元素成功
     */
    bool pop(T &item)
    {
        m_mutex.lock();
        while(m_size <= 0)
        {
            if(!m_cond.wait(m_mutex.getlock()))
            {
                //不等于0
                m_mutex.unlock();
                return false;
            }
        }
        m_front = (m_front + 1) % m_max_size;
        item = m_array[m_front];
        m_size--;
        m_mutex.unlock();
        return true;
    }
    bool pop(T &item,int ms_timeout)
    {
        struct timespec t = {0,0};
        struct timeval now = {0,0};
        gettimeofday(&now,NULL);
        m_mutex.lock();
        if(m_size <= 0)
        {
            t.tv_sec = now.tv_sec + ms_timeout / 1000;
            t.tv_nsec = (ms_timeout % 1000) * 1000;
            if (!m_cond.timewait(m_mutex.getlock(), t))
            {
                m_mutex.unlock();
                return false;
            }
        }
        if (m_size <= 0)
        {
            m_mutex.unlock();
            return false;
        }

        m_front = (m_front + 1) % m_max_size;
        item = m_array[m_front];
        m_size--;
        m_mutex.unlock();
        return true;
    }
private:
    //! 互斥锁
    locker m_mutex;
    //! 条件变量
    cond m_cond;
    //! 阻塞队列的元素数组
    T *m_array;
    //! 队列最大值
    int m_max_size;
    //! 队列当前大小
    int m_size;
    //! 队列的头
    int m_front;
    //! 队列尾
    int m_back;

};



#endif //WEBPROJECT_BLOCK_QUEUE_H
