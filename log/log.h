//
// Created by chencaikun on 2023/8/24.
//

#ifndef WEBPROJECT_LOG_H
#define WEBPROJECT_LOG_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <stdarg.h>
#include <pthread.h>
#include "lock/locker.h"
#include "block_queue.h"

using namespace std;

class Log
{
public:
    /**
     * @brief 懒汉单例模式，在c++11之后懒汉模式不再需要加锁，获取单例模式的日志实例
     * @return
     */
    static Log* get_instance()
    {
        static Log instance;
        return &instance;
    }
    /**
     * @brief 异步写日志公有方法，调用私有方法async_write_log
     * @param args
     * @return
     */
    static void* flush_log_thread(void *args)
    {
        Log::get_instance()->async_write_log();
    }
    /**
     * @brief 初始化日志实例
     * @param file_name 日志文件
     * @param close_log 是否关闭日志
     * @param log_buf_size 日志缓冲区的大小
     * @param split_line 单个日志文件最大行数
     * @param max_queue_size 阻塞队列的最大日志条数,异步才需要设置，同步不需要
     * @return
     */
    bool init(const char * file_name, int close_log,int log_buf_size = 8192,int split_line = 5000000,int max_queue_size = 0);
    /**
     * @brief 将输出到日志的内容按照标准格式整理
     * @param level 日志等级
     * @param format 可变参数
     * @param ...
     */
    void write_log(int level,const char *format,...);
    /**
     * @brief 强制刷新缓冲区
     */
    void flush(void);

private:
    /**
     * @brief 单例模式下要私有化构造函数
     */
    Log();
    /**
     * @brief 析构函数
     */
    virtual ~Log();
    /**
     * @brief 异步写入日志的方法
     * @return
     */
    void* async_write_log()
    {
        string single_log;
        //从阻塞队列取出一个日志信息string,写入日志文件
        while(m_log_queue->pop(single_log))
        {
            m_mutex.lock();
            fputs(single_log.c_str(),m_fp);
            m_mutex.unlock();
        }
    }

private:
    //! 路径名
    char dir_name[128];
    //! Log文件名
    char log_name[128];
    //! 日志文件最大行数
    int m_split_lines;
    //! 日志缓冲区大小
    int m_log_buf_size;
    //! 所有日志行数记录
    long long m_count;
    //! 按天分类，记录当前日期
    int m_today;
    //! 打开log的文件指针
    FILE *m_fp;
    //! 暂存要写入日志的信息
    char *m_buf;
    //! 阻塞队列
    block_queue<string> *m_log_queue;
    //! 同步还是异步的标志位，true表示异步
    bool m_async;
    //! 锁
    locker m_mutex;
    //! 关闭日志
    int m_close_log;
};

//定义打印日志的宏 format是格式字符串，...是参数列表
#define LOG_DEBUG(format, ...) if(0 == m_close_log) {Log::get_instance()->write_log(0, format, ##__VA_ARGS__); Log::get_instance()->flush();}
#define LOG_INFO(format, ...) if(0 == m_close_log) {Log::get_instance()->write_log(1, format, ##__VA_ARGS__); Log::get_instance()->flush();}
#define LOG_WARN(format, ...) if(0 == m_close_log) {Log::get_instance()->write_log(2, format, ##__VA_ARGS__); Log::get_instance()->flush();}
#define LOG_ERROR(format, ...) if(0 == m_close_log) {Log::get_instance()->write_log(3, format, ##__VA_ARGS__); Log::get_instance()->flush();}

#endif //WEBPROJECT_LOG_H
