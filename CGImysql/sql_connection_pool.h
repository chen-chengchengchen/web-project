//
// Created by chencaikun on 2023/8/27.
//

#ifndef WEBPROJECT_SQL_CONNECTION_POOL_H
#define WEBPROJECT_SQL_CONNECTION_POOL_H
#include <stdio.h>
#include <list>
#include <mysql/mysql.h>
#include <error.h>
#include <string.h>
#include <iostream>
#include <string>
#include "../lock/locker.h"
#include "../log/log.h"

using namespace std;

/**
 * @brief 使用单例模式的数据库连接池
 */
class connection_pool
{
public:
    /**
     *@brief 获取数据库连接
     * @return 返回一个连接好的mysql
     */
    MYSQL *GetConnection();
    /**
     * @brief 释放数据库连接
     * @param con 要释放的连接
     * @return 是否释放成功
     */
    bool ReleaseConnection(MYSQL *con);
    /**
     * @brief 获取还有多少个空闲的数据库连接
     * @return 返回空闲的连接个数
     */
    int GetFreeConn();
    /**
     * @brief 销毁所有数据库连接
     */
    void DestroyPool();
    /**
     * @brief 单例模式 用局部静态变量的懒汉模式实现
     * @return
     */
    static connection_pool* GetInstance();
    /**
     * @brief 初始化数据库池
     * @param url 主机地址（IP）
     * @param User 数据库登录的用户名
     * @param Password 数据库的登陆密码
     * @param DatabaseName 使用的数据库名
     * @param Port 数据库端口号
     * @param MaxConn 最大的连接数
     * @param close_log 是否关闭日志
     */
    void init(string url,string User,string Password,string DatabaseName,int Port,int MaxConn,int close_log);

private:
    /**
     * @brief 单例模式要私有化构造函数
     */
    connection_pool();
    /**
     * @brief 私有化析构函数
     */
    ~connection_pool();

    //! 最大数据库连接数
    int m_MaxConn;
    //! 当前已使用连接
    int m_CurConn;
    //! 当前空闲连接数
    int m_FreeConn;
    //! 数据库池的使用锁
    locker lock;
    //! 连接池
    list<MYSQL*> connList;
    //! 信号量机制来获取和释放连接
    sem reserve;
private:
    //! 主机地主 IP地址
    string m_url;
    //! 数据库端口号
    int m_port;
    //! 登录数据库的用户名
    string m_User;
    //! 登录数据库的用户密码
    string m_Password;
    //! 数据库名
    string m_DatabaseName;
    //! 日志开关
    int m_close_log;
};


/**
 * @brief 使用RAII机制来管理mysql连接的声明和销毁，生命周期即使对象的声明周期
 */
class connectionRAII
{
public:
    /**
     * @brief 构造函数 获取一个数据库连接
     * @param conn 存放获取到的数据库连接
     * @param connPool 数据库连接池的单例模式
     */
    connectionRAII(MYSQL **conn,connection_pool *connPool);
    /**
     * @brief 析构函数
     */
    ~connectionRAII();
private:
    //! 获取到的数据库连接
    MYSQL *connRAII;
    //! 数据库连接池的单例模式
    connection_pool *poolRAII;
};


#endif //WEBPROJECT_SQL_CONNECTION_POOL_H
