//
// Created by chencaikun on 2023/8/27.
//

#include <mysql/mysql.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <list>
#include <pthread.h>
#include <iostream>
#include "sql_connection_pool.h"

using namespace std;

connection_pool::connection_pool()
{
    m_CurConn = 0;
    m_FreeConn = 0;
}

connection_pool::~connection_pool()
{
    DestroyPool();
}

connection_pool *connection_pool::GetInstance()
{
    static connection_pool instance;
    return &instance;
}

void connection_pool::init(std::string url, std::string User, std::string Password, std::string DatabaseName, int Port,
                           int MaxConn, int close_log)
{
        m_url = url;
        m_User = User;
        m_Password = Password;
        m_port = Port;
        m_DatabaseName = DatabaseName;
        m_close_log = close_log;

        for(int i = 0;i < MaxConn ;i++)
        {
            MYSQL *conn = NULL;
            conn = mysql_init(conn);

            if(conn == NULL)
            {
                LOG_ERROR("MySQL Error");
                exit(1);
            }
            conn = mysql_real_connect(conn,url.c_str(),User.c_str(),Password.c_str(),DatabaseName.c_str(),Port,NULL,0);
            if(conn == NULL)
            {
                LOG_ERROR("MySQL Error");
                exit(1);
            }
            connList.push_back(conn);
            ++m_FreeConn;
        }
        reserve = sem(m_FreeConn);

        m_MaxConn = m_FreeConn;
}

MYSQL *connection_pool::GetConnection()
{
    MYSQL *conn;
    if(connList.size() == 0)
    {
        return NULL;
    }

    reserve.wait();

    lock.lock();
    conn = connList.front();
    connList.pop_front();

    --m_FreeConn;
    ++m_CurConn;
    lock.unlock();

    return conn;
}

bool connection_pool::ReleaseConnection(MYSQL *con)
{
    if(NULL == con)
    {
        return false;
    }

    lock.lock();

    connList.push_back(con);
    ++m_FreeConn;
    --m_CurConn;

    lock.unlock();
    reserve.post();
    return true;
}

void connection_pool::DestroyPool()
{
    lock.lock();
    if(connList.size() > 0)
    {
        list<MYSQL *>::iterator it;
        for(it = connList.begin(); it != connList.end();it++)
        {
            MYSQL *con = *it;
            mysql_close(con);
        }
        m_CurConn = 0;
        m_FreeConn = 0;
        connList.clear();
    }
    lock.unlock();
}


int connection_pool::GetFreeConn()
{
    return this->m_FreeConn;
}

connectionRAII::connectionRAII(MYSQL **conn, connection_pool *connPool)
{
    *conn = connPool->GetConnection();
    connRAII = *conn;
    poolRAII = connPool;
}

connectionRAII::~connectionRAII()
{
    poolRAII->ReleaseConnection(connRAII);
}