//
// Created by chencaikun on 2023/8/21.
//

#ifndef WEBPROJECT_LOCKER_H
#define WEBPROJECT_LOCKER_H

#include <exception>
#include <pthread.h>
#include <semaphore.h>

class locker
{
public:
    /**
     * @brief 初始化锁
     */
    locker()
    {
        if(pthread_mutex_init(&m_mutex,NULL) != 0)
        {
            //初始化出错抛出异常
            throw std::exception();
        }
    }
    /**
     * @brief 销毁锁
     */
    ~locker()
    {
        pthread_mutex_destroy(&m_mutex);
    }
    /**
     * @brief 加锁
     * @return 返回锁类型的指针
     */
    bool lock()
    {
        return pthread_mutex_lock(&m_mutex) == 0;
    }
    /**
     * @brief 释放锁
     * @return 解锁是否成功
     */
    bool unlock()
    {
        return pthread_mutex_unlock(&m_mutex) == 0;
    }
    /**
     * @brief 获取锁
     * @return 返回锁类型的指针
     */
    pthread_mutex_t* getlock(){
        return &m_mutex;
    }
private:
    //！ 互斥锁
    pthread_mutex_t m_mutex;
};


class sem
{
public:
    /**
     * @brief 构造函数，初始化信号量,信号量为0，说明该开始wait会阻塞，可用于线程同步
     */
    sem()
    {
        if(sem_init(&m_sem,0,0) != 0)
        {
            throw std::exception();
        }
    }
    /**
     * @brief 构造函数，初始胡信号量
     * @param num 信号量的初始值
     */
    sem(int num)
    {
        if(sem_init(&m_sem,0,num) != 0)
        {
            throw std::exception();
        }
    }
    /**
     * @brief 析构函数,销毁信号
     */
    ~sem()
    {
        sem_destroy(&m_sem);
    }
    /**
     * @brief 信号量的P操作
     * @return
     */
    bool wait()
    {
        return sem_wait(&m_sem) == 0;
    }
    /**
     * @brief 信号量的V操作
     * @return
     */
    bool post()
    {
        return sem_post(&m_sem) == 0;
    }
private:
    //! 信号量
    sem_t m_sem;
};

class cond
{
public:
    /**
     * @brief 构造函数，初始化条件变量
     */
    cond()
    {
        if(pthread_cond_init(&m_cond,NULL) != 0)
        {
            throw std::exception();
        }
    }
    /**
     * @brief 析构函数
     */
    ~cond()
    {
        pthread_cond_destroy(&m_cond);
    }
    /**
     * @brief 等待条件变量
     * @param mutex 配合条件变量使用的互斥锁
     * @return
     */
    bool wait(pthread_mutex_t *mutex)
    {
        int ret = 0;
        ret = pthread_cond_wait(&m_cond,mutex);
        return ret == 0;
    }
    /**
     *@brief 等待条件变量，时间到会结束等待
     * @param mutex 互斥锁
     * @param t 一个时间的结构体，表示秒和纳秒
     * @return
     */
    bool timewait(pthread_mutex_t *mutex , struct timespec t)
    {
        int ret = 0;
        ret = pthread_cond_timedwait(&m_cond,mutex,&t);
        return ret ==0;
    }
    /**
     * @brief 唤醒某个等待条件变量的线程
     * @return
     */
    bool signal()
    {
        return pthread_cond_signal(&m_cond) == 0;
    }
    /**
     * @brief 唤醒所有等待该条件变量的信号
     * @return
     */
    bool broadcast()
    {
        return pthread_cond_broadcast(&m_cond) ==0;
    }
private:
    //! 条件变量
    pthread_cond_t m_cond;
};


#endif //WEBPROJECT_LOCKER_H
