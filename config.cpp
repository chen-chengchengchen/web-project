//
// Created by cheng on 23-9-5.
//

#include "config.h"

Config::Config()
{
    //端口号默认是9006
    PORT = 9006;
    //日志写入方式，默认同步
    LOGWrite = 0;
    //触发组合模式 默认listenfd LT + connfd LT
    TRIGMode = 0;
    LISTENTrigmode = 0;
    CONNTrigmode = 0;

    //优雅关闭默认不使用
    OPT_LINGER = 0;
    //数据库连接池的数量默认是8
    sql_num = 8;
    //线程池的数量默认是8
    thread_num = 8;
    //默认打开日志
    close_log = 0;
    //并发模型默认是proactor
    actor_model = 0;
}

void Config::parse_arg(int argc, char **argv)
{
    int opt;
    const char *str = "p:l:m:o:s:t:c:a:";
    //etopt 函数会迭代处理命令行参数，并返回下一个选项字符。使用 switch 语句来处理不同的选项，并获取相应的参数
    //在 str 字符串中，每个选项字母可以带有一个冒号 :，表示该选项需要接收参数。如果选项后面不带冒号，则表示该选项不接收参数。
    while((opt = getopt(argc,argv,str)) != -1)
    {
        switch (opt)
        {
            case 'p':
            {
                PORT = atoi(optarg);
                break;
            }
            case 'l':
            {
                LOGWrite = atoi(optarg);
                break;
            }
            case 'm':
            {
                TRIGMode = atoi(optarg);
                break;
            }
            case 'o':
            {
                OPT_LINGER = atoi(optarg);
                break;
            }
            case 's':
            {
                sql_num = atoi(optarg);
                break;
            }
            case 't':
            {
                thread_num = atoi(optarg);
                break;
            }
            case 'c':
            {
                close_log = atoi(optarg);
                break;
            }
            case 'a':
            {
                actor_model = atoi(optarg);
                break;
            }
            default:
                break;
        }
    }
}