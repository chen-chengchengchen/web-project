//
// Created by chencaikun on 2023/8/21.
//
#include "lock/locker.h"
#include "log/block_queue.h"
#include "CGImysql/sql_connection_pool.h"
#include "log/log.h"
#include "http/http_conn.h"
#include "webserver.h"
#include "threadpool/threadpool.h"
#include "timer/lst_timer.h"
#include<stdio.h>
#include<pthread.h>
#include<iostream>
#include<unistd.h>
#include<thread>
#include "config.h"
#include "timer/heaptimer.h"

int main(int argc, char *argv[])
{
    //需要修改的数据库信息,登录名,密码,库名
    string user = "root";
    string passwd = "cck20000818";
    string databasename = "yourdb";

    Config config;
    config.parse_arg(argc,argv);
    WebServer server;

    //初始化
    server.init(config.PORT, user, passwd, databasename, config.LOGWrite,
                config.OPT_LINGER, config.TRIGMode,  config.sql_num,  config.thread_num,
                config.close_log, config.actor_model);


    //日志
    server.log_write();

    //数据库
    server.sql_pool();

    //线程池
    server.thread_pool();

    //触发模式
    server.trig_mode();

    //监听
    server.eventListen();

    //运行
    server.eventLoop();

    return 0;
}