# WebProject

#### 介绍
一个Linux高性能服务器。

WebProject 商城管理服务器

Linux下C++轻量级Web服务器

背景：通过服务器，实现多客户端对商城物品的增删改查的管理 结合html和webserver服务器实现对商城系统进行管理

实现：

使用 线程池 + 非阻塞socket + epoll(ET/LT) + 事件处理(Reactor和模拟Proactor均实现) 的并发模型。
状态机解析HTTP请求报文，支持解析GET和POST请求,实现客户端获取和处理数据。
访问服务器数据库实现web端用户注册、登录功能，可以对商城的数据进行管理。
实现同步/异步日志系统，记录服务器运行状态。
经Webbench压力测试可以实现上万的并发连接数据交换。

性能：

    机器环境：Ubuntu 20.04 64bit CPU4核 内存8GB

    QPS: 11000+

    测试环境：Webbench 并发数为20000

效果图：
![img.png](img.png)

前端设计：

    登录界面设计:
![img_1.png](img_1.png)

    数据查询与展示页面设计
![img_2.png](img_2.png)

    数据添加页面设计
![img_3.png](img_3.png)


运行程序

./WebProject -p 9007 -l 1 -m 0 -o 1 -s 10 -t 10 -c 1 -a 1

端口9007

异步写入日志

使用LT + LT组合

使用优雅关闭连接

数据库连接池内有10条连接

线程池内有10条线程

关闭日志

Reactor反应堆模型