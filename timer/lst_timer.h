//
// Created by cheng on 23-8-28.
//

#ifndef WEBPROJECT_LST_TIMER_H
#define WEBPROJECT_LST_TIMER_H
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <sys/stat.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/uio.h>

#include<time.h>
#include "../log/log.h"

class util_timer;

/**
 * @brief 存放数据的结构体 （连接资源）
 */
struct client_data
{
    //! socket套接字
    sockaddr_in address;
    //！ 套接字对应的文件描述符
    int sockfd;
    //! 定时器
    util_timer *timer;
};

/**
 * @brief 定时任务类
 */
class util_timer
{
public:
    util_timer() : prev(NULL),next(NULL) {}

public:
    //！ 超时时间
    time_t expire;

    //！ 回调函数，参数是client_data类型
    void (* cb_func)(client_data *);
    //! 用户数据
    client_data *user_data;
    //! 前一个定时器
    util_timer *prev;
    //！ 后一个定时器
    util_timer *next;

};


/**
 * @brief 定时器链表
 */
class sort_timer_lst
{
public:
    /**
     * @brief 构造函数
     */
    sort_timer_lst();
    /**
     * @brief 析构函数
     */
    ~sort_timer_lst();

    /**
     * @brief 增加一个定时器
     * @param timer 要增增加的定时器
     */
    void add_timer(util_timer *timer);
    /**
     * @brief 调整定时器，只考虑定时时间往后延迟的情况
     * @param timer 要调整的定时器
     */
    void adjust_timer(util_timer *timer);
    /**
     * @brief 删除某个定时器
     * @param timer 要删除的定时器
     */
    void del_timer(util_timer *timer);
    /**
     * @brief 脉搏函数
     */
    void tick();
    /**
     * @brief 打印升序链表信息
     */
    void show();

private:
    /**
     * @brief 往链表中增加一个定时器
     * @param timer 新增的定时器
     * @param lst_head 定时器链表
     */
    void add_timer(util_timer *timer,util_timer *lst_head);

    //! 链表的头节点
    util_timer *head;
    //！ 链表的尾节点
    util_timer *tail;
};

/**
 * @brief 工具类
 */
class Utils
{
public:
    /**
     * @brief 构造函数
     */
    Utils(){}
    /**
     * @brief 析构函数
     */
    ~Utils(){}

    /**
     * @brief 初始化定时任务
     * @param timeslot 定时时间
     */
    void init(int timeslot);
    /**
     * @brief 将文件描述符设置为非阻塞
     * @param fd 文件描述符
     * @return
     */
    int setnonblocking(int fd);
    /**
     * @brief 将内核事件表注册读事件，ET模式选择开启EPOLLONESHOT
     * @param epollfd 事件表文件描述符
     * @param fd 要监听的文件描述符
     * @param one_shot 是否开启 EPOLLONESHOT模式
     * @param TRIGMode
     */
    void addfd(int epollfd,int fd,bool one_shot,int TRIGMode);
    /**
     * @brief 信号处理函数
     * @param sig 信号
     */
    static void sig_handler(int sig);
    /**
     * @brief 设置信号处理函数
     * @param sig 设置的信号
     * @param 第二个参数是要指向的处理函数，void返回类型，型参列表为一个int类型
     * @param restart 是否使被信号打断的系统调用自动重新发起
     */
    void addsig(int sig,void(handler)(int),bool restart = true);
    /**
     * @brief 定时处理任务，重新定时以不断触发SIGALRM信号
     */
    void timer_handler();
    /**
     * @brief 将错误信息发送回对应的socket连接，之后关闭sokect
     * @param connfd
     * @param info
     */
    void show_error(int connfd,const char *info);
public:
    //! 管道描述符
    static int *u_pipefd;
    //！ 定时器链表
    sort_timer_lst m_timer_lst;
    //！ epollIO文件描述发
    static int u_epollfd;
    //! 脉搏函数的心跳间隔
    int  m_TIMESLOT;
};

/**
 * @brief 定时器回调函数
 * @param user_data 用户信息
 */
void cb_func(client_data *user_data);

#endif //WEBPROJECT_LST_TIMER_H
