//
// Created by chencaikun on 2023/9/9.
//

#include "heaptimer.h"

time_heap::time_heap(int cap)
{
    m_capacity = cap;
    m_cur_size = 0;
    //创建堆数组
    m_array = new heap_timer * [m_capacity];
    if(!m_array)
    {
        throw std::exception();
    }
    for(int i = 0; i < m_capacity; i++)
    {
        m_array[i] = NULL;
    }
}

time_heap::time_heap(heap_timer **init_array, int size, int capacity)
{
    m_cur_size = size;
    m_capacity = capacity;
    if(capacity < size)
    {
        throw std::exception();
    }
    //创建堆数组
    m_array = new heap_timer * [capacity];
    if(!m_array)
    {
        throw std::exception();
    }
    for(int i = 0;i < capacity ;i++)
    {
        m_array[i] = NULL;
    }
    if(size != 0)
    {
        //初始化堆数组
        for(int i = 0;i < size ;i++)
        {
            m_array[i] = init_array[i];
        }
        for(int i = (m_cur_size - 1)/2 ;i >= 0; i--)
        {
            // 对堆的前（cur_size -  1）/2的元素（即使非叶子节点）进行下滤操作，即可得到一个完整堆
            percolate_down(i);
        }
    }
}

time_heap::~time_heap()
{
    for(int i = 0; i < m_cur_size; i++)
    {
        delete m_array[i];
    }
    delete [] m_array;
}

void time_heap::add_timer(heap_timer *timer)
{
    if(!timer)
    {
        return ;
    }
    //如果堆容量不够需要进行扩容
    if(m_cur_size >= m_capacity)
    {
        resize();
    }
    //新插入了一个元素，当前堆的大小加1，hole是新建的空穴的位置
    int hole = m_cur_size++;
    //空穴的父节点
    int parent = 0;
    // 对空穴到根节点的路径上所有的节点执行上滤操作
    for(;hole >0 ;hole = parent)
    {
        parent = (hole-1)/2;
        if(m_array[parent]->expire <= timer->expire)
        {
            break;
        }
        //将空穴和父节点交换位置
        m_array[hole] = m_array[parent];
    }
    //找对这个timer应该插入的位置
    m_array[hole] = timer;
}

void time_heap::del_timer(heap_timer *timer)
{
    if(!timer)
    {
        return ;
    }
    //仅仅将目标定时的回调函数设置为空，即所谓的延迟销毁，这将节省真正删除该定时器的开销，但是这样做容易使堆数组膨胀
    timer->cb_func = NULL;
}

heap_timer *time_heap::top() const
{
    if( empty() )
    {
        return NULL;
    }
    return m_array[0];
}

void time_heap::pop_timer()
{
    if( empty() )
    {
        return ;
    }
    if(m_array[0])
    {
        delete m_array[0];
        //将原来的堆顶元素替换为堆数组中最后应该元素
        if(m_cur_size == 1)
        {
            return ;
        }
        m_array[0] = m_array[--m_cur_size];
        //对新的堆顶元素执行下滤操作
        percolate_down(0);
    }
}


void time_heap::tick()
{
    heap_timer* tmp = m_array[0];
    time_t cur = time(NULL);
    //循环处理堆中到期的定时器
    while(! empty() )
    {
        if(!tmp)
        {
            return ;
        }
        //堆顶还没到期
        if(tmp->expire  > cur)
        {
            break;
        }
        //到期了就执行定时器的任务
        if(m_array[0]->cb_func)
        {
            m_array[0]->cb_func(m_array[0]->user_data);
        }
        //将堆顶元素删除，同时生成新的堆顶元素
        pop_timer();
        tmp = m_array[0];
    }
}


void time_heap::percolate_down(int hole)
{
    heap_timer* temp = m_array[hole];
    int child = 0;
    for(; (hole*2+1) <= (m_cur_size-1);hole = child)
    {
        child = hole * 2 + 1;
        if(child < (m_cur_size -1 ) && m_array[child+1]->expire < m_array[child]->expire )
        {
            child++;
        }
        if(m_array[child]->expire < temp->expire)
        {
            m_array[hole] = m_array[child];
        }
        else
        {
            break;
        }
    }
    m_array[hole] = temp;
}

void time_heap::resize()
{
    heap_timer** temp = new heap_timer * [2*m_capacity];
    for(int i = 0; i < 2*m_cur_size ;i++)
    {
        temp[i] = NULL;
    }
    if(! temp)
    {
        throw std::exception();
    }
    m_capacity = 2 * m_capacity;
    for(int i = 0; i < m_capacity ;i++)
    {
        temp[i] = m_array[i];
    }
    delete [] m_array;
    m_array = temp;
}

void time_heap::print()
{
    for(int i = 0; i < m_cur_size ;i++)
    {
        printf("%d ",m_array[i]->expire);
    }
    printf("\n");
}
