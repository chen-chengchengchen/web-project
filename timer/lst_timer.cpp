//
// Created by cheng on 23-8-28.
//

#include "lst_timer.h"

sort_timer_lst::sort_timer_lst()
{
    head = NULL;
    tail = NULL;
}

sort_timer_lst::~sort_timer_lst()
{
    util_timer *tmp = head;
    while( tmp != NULL)
    {
        head = tmp->next;
        delete tmp;
        tmp = head;
    }
}

void sort_timer_lst::add_timer(util_timer *timer)
{
    if(!timer)
    {
        return ;
    }
    if(!head)
    {
        head = tail = timer;
        return ;
    }
    //插入头节点
    if(timer->expire < head->expire)
    {
        timer->next = head;
        head->prev = timer;
        head = timer;
        return ;
    }
    //插入中间的情况
    add_timer(timer,head);
}

void sort_timer_lst::adjust_timer(util_timer *timer)
{
    if(!timer)
    {
        return ;
    }
    //不用调整的情况
    util_timer *tmp = timer->next;
    if(!tmp || timer->expire < tmp->expire)
    {
        return ;
    }

    if(timer == head)
    {
        head = head->next;
        head->prev = NULL;
        timer->next = NULL;
        add_timer(timer);
    }
    else
    {
        timer->prev->next = timer->next;
        timer->next->prev = timer->prev;
        add_timer(timer,timer->next);
    }
}

void sort_timer_lst::del_timer(util_timer *timer)
{
    if(!timer)
    {
        return ;
    }
    if(timer == head && timer == tail)
    {
        head = tail = NULL;
        delete timer;
        return ;
    }
    if(timer == head)
    {
        head = head->next;
        head->prev =  NULL;
        delete timer;
        return ;
    }
    if(timer == tail)
    {
        tail = tail->prev;
        tail->next = NULL;
        delete timer;
        return ;
    }
    timer->next->prev = timer->prev;
    timer->prev->next = timer->next;
    delete timer;
}

void sort_timer_lst::tick()
{
    if(!head)
    {
        return ;
    }
    time_t cur = time(NULL);
    util_timer *tmp = head;
    while(tmp)
    {
        if(tmp->expire > cur)
        {
            break;
        }
        //执行回调函数
        tmp->cb_func(tmp->user_data);
        head = head->next;
        if(head)
        {
            head->prev = NULL;
        }
        delete tmp;
        tmp = head;
    }
}

void sort_timer_lst::add_timer(util_timer *timer, util_timer *lst_head)
{
    util_timer *prev = lst_head;
    util_timer *tmp = prev->next;
    while(tmp)
    {
        if(tmp->expire > timer->expire)
        {
            prev->next = timer;
            timer->prev = prev;
            timer->next = tmp;
            tmp->prev = timer;
            break;
        }
        prev = tmp;
        tmp = tmp->next;
    }
    if(!tmp)
    {
        prev->next = timer;
        timer->prev = prev;
        timer->next = NULL;
        tail = timer;
    }
}

void sort_timer_lst::show()
{
    util_timer *tmp = head;
    while(tmp)
    {
        printf("%lld ",tmp->expire);
        tmp = tmp->next;
    }
    printf("\n");
}

void Utils::init(int timeslot)
{
    m_TIMESLOT = timeslot;
}


int Utils::setnonblocking(int fd)
{
    int old_option = fcntl(fd,F_GETFL);
    int new_option = old_option | O_NONBLOCK;
    fcntl(fd,new_option);
    return old_option;
}

void Utils::addfd(int epollfd, int fd, bool one_shot, int TRIGMode)
{
    epoll_event event;
    event.data.fd = fd;

    if(1 == TRIGMode) //ET边缘触发模式
    {
        event.events =  EPOLLIN | EPOLLET | EPOLLRDHUP;
    }
    else //LT水平触发模式
    {
        event.events =  EPOLLIN | EPOLLRDHUP;
    }

    if(one_shot) // 启用EPOLLONESHOT
    {
        event.events |= EPOLLONESHOT;
    }
    epoll_ctl(epollfd,EPOLL_CTL_ADD,fd,&event);
    setnonblocking(fd);
}

void Utils::sig_handler(int sig)
{
    //为了保证可重入性，保存原来的errno
    int save_errno = errno;
    int msg = sig;
    send(u_pipefd[1], (char *)&msg, 1, 0);
    errno = save_errno;
//    printf("收到信号:%d\n",sig);
}

void Utils::addsig(int sig, void (*handler)(int), bool restart)
{
    struct sigaction sa;
    memset(&sa,'\0',sizeof(sa));
    sa.sa_handler = handler;
    if(restart)
    {
        //使被信号打断的系统调用自动重新发起
        sa.sa_flags |= SA_RESTART;
    }
    sigfillset(&sa.sa_mask);
    assert(sigaction(sig,&sa,NULL) != -1);
}

void Utils::timer_handler()
{
    m_timer_lst.tick();
    // 重新定时以不断触发SIGALRM信号
    alarm((m_TIMESLOT));
}

void Utils::show_error(int connfd, const char *info)
{
    send(connfd,info, strlen(info),0);
    close(connfd);
}

int *Utils::u_pipefd = 0;
int Utils::u_epollfd = 0;

class Utils;


void cb_func(client_data *user_data)
{
    epoll_ctl(Utils::u_epollfd,EPOLL_CTL_DEL,user_data->sockfd,0);
    assert(user_data);
    close(user_data->sockfd);
    //未来用到
    //http_conn::m_user_count--;
}
