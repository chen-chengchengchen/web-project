//
// Created by chencaikun on 2023/9/9.
//

#ifndef WEBPROJECT_HEAPTIMER_H
#define WEBPROJECT_HEAPTIMER_H

#include <iostream>
#include <netinet/in.h>
#include <time.h>
#include <exception>

#define BUFFER_SIZE 64
#include "../log/log.h"

//前向声明
class heap_timer;

/**
 * @brief 绑定socket和定时器
 */
struct heap_client_data {
    sockaddr_in address;
    int sockfd;
    char buf[BUFFER_SIZE];
    heap_timer *timer;
};


/**
 * @brief 堆定时器类
 */
class heap_timer
{
public:
    /**
     * @brief 定时器的构造函数
     * @param delay  几秒后生效
     */
    heap_timer(int delay)
    {
        expire =  delay;
    }

public:
    //! 定时器生效的绝对时间
    time_t expire;
    //! 定时器的回调函数  定义了一个指针 cb_func，它指向一个函数，该函数的参数类型是 client_data *，返回类型为 void
    void (*cb_func)(heap_client_data *);
    //! 用户数据
    heap_client_data* user_data;
};

/**
 * @brief 时间堆类
 */
class time_heap
{
public:
    /**
     * @brief 构造函数之一，初始化一个大小为cap的空堆
     * @param cap
     */
    time_heap(int cap);
    /**
     * @brief 构造函数之二
     * @param init_array 用已有的数组来初始化堆
     * @param size 数组的大小
     * @param capacity 堆数组的容量
     */
    time_heap(heap_timer** init_array, int size, int capacity);
    /**
     * @brief 销毁时间堆
     */
    ~time_heap();

public:
    /**
     * @brief 添加目标定时器
     * @param timer 需要添加的定时器
     */
    void add_timer(heap_timer* timer);
    /**
     * @brief 删除目标定时器
     * @param timer 需要删除的定时器
     */
    void del_timer(heap_timer* timer);
    /**
     * @brief 获取堆顶部的定时器
     * @return
     */
    heap_timer* top() const;
    /**
     * @brief 删除堆顶部的定时器
     */
    void pop_timer();
    /**
     * @brief 心搏函数
     */
    void tick();
    /**
     * @brief 判断时间堆是否为空
     * @return
     */
    bool empty() const { return m_cur_size == 0; }
    /**
     * @brief 打印堆数组的所有定时器
     */
    void print();
private:
    /**
     * @brief 最小堆的下滤操作，它确保数组堆中第hole个节点作为根的子树拥有最小堆的性质
     * @param hole
     */
    void percolate_down(int hole);
    /**
     * @brief 将堆数组容量扩大一倍
     */
    void resize();

private:
    //! 堆数组 指向指针的指针
    heap_timer** m_array;
    //! 堆数组的容量
    int m_capacity;
    //! 堆数组当前包含元素的个数
    int m_cur_size;
};

#endif //WEBPROJECT_HEAPTIMER_H
