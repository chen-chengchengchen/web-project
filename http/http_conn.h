//
// Created by cheng on 23-8-29.
//

#ifndef WEBPROJECT_HTTP_CONN_H
#define WEBPROJECT_HTTP_CONN_H

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <sys/stat.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/uio.h>
#include <map>

#include "../lock/locker.h"
#include "../CGImysql/sql_connection_pool.h"
#include "../timer/lst_timer.h"
#include "../log/log.h"
#include "../timer/heaptimer.h"

class http_conn
{
public:
    //! 设置读取文件的名称m_real_file大小
    static const int FILENAME_LEN = 200;
    //! 设置读缓冲区的m_read_buf大小
    static const int READ_BUFFER_SIZE = 2048;
    //! 设置写缓冲区m_write_buf的大小
    static const int WRITE_BUFFER_SIZE = 2048;

    //！ 报文的请求方法，本项目只是用到Get和POST
    enum METHOD
    {
        GET = 0,
        POST,
        HEAD,
        PUT,
        DELETE,
        TRACE,
        OPTIONS,
        CONNECT,
        PATH
    };
    //！主状态机的状态
    enum CHECK_STATE
    {
        //！ 解析请求行
        CHECK_STATE_REQUESTLINE = 0,
        //! 解析请求头部
        CHECK_STATE_HEADER,
        //！ 解析请求数据
        CHECK_STATE_CONTENT
    };
    //! 报文解析的结果
    enum HTTP_CODE
    {
        NO_REQUEST,
        GET_REQUEST,
        BAD_REQUEST,
        NO_RESOURCE,
        FORBIDDEN_REQUEST,
        FILE_REQUEST,
        INTERNAL_ERROR,
        CLOSED_CONNECTION
    };
    //! 从状态机的状态
    enum LINE_STATUS
    {
        //！ 读取到一行
        LINE_OK = 0,
        //! 语法有误
        LINE_BAD,
        //! 没有读取完整，请继续读取内容
        LINE_OPEN
    };

public:
    /**
     * @brief 构造函数
     */
    http_conn() {}
    /**
     * @brief 析构函数
     */
    ~http_conn() {}

public:
    /**
     * @brief 初始化套接字地址，内部函数会调用私有的init
     * @param sockfd  套接字的文件描述符
     * @param addr
     * @param user
     * @param passwd
     * @param sqlname
     */
    void init(int sockfd, const sockaddr_in &addr, char *,int,int,string user,string passwd,string sqlname);
    /**
     * @brief 关闭http连接
     * @param real_close
     */
    void close_conn(bool real_close = true);
    /**
     * @brief 工作队列取出任务后执行的处理函数
     */
    void process();
    /**
     * @brief 读取浏览器端发来的全部数据 一次性读完，直到无数据可读或者对方关闭连接
     * @return
     */
    bool read_once();
    /**
     * @brief 响应报文写入函数
     * @return 是否写入成功
     */
    bool write();
    /**
     * @brief 获取套接字地址
     * @return 返回套接字地址
     */
    sockaddr_in *get_address()
    {
        return &m_address;
    }
    /**
     * @brief 同步线程初始化数据库读取表,就是获取数据库用户表中的所有用户
     * @param connPool
     */
    void initmysql_result(connection_pool *connPool);
    /**
     * @brief CGI使用线程池初始化数据库表
     * @param connPool
     * @return
     */
    void initresultFile(connection_pool *connPool);

    //! 用来标识是否删除这个timer和关闭socket连接
    int timer_flag;
    //! 类似状态机，0表示主线程通知工作线程有可读事件，可读时间完成之后把它改为1,表示是可写事件
    int improv;

private:
    /**
     * @brief 私有的初始化套接字函数 主要是初始化一些私有的数据，初始化http请求的请求参数等
     */
    void init();
    /**
     * @brief 从没m_read_buf读取数据，并处理请求报文
     * @return 返回处理结果
     */
    HTTP_CODE process_read();
    /**
     * @brief 向m_write_buf写入响应报文数据
     * @param ret
     * @return
     */
    bool process_write(HTTP_CODE ret);
    /**
     * @brief 主状态机解析报文中的请求行数据 ,解析http请求行，获得请求方法，目标url及http版本号
     * @param text 要解析的字符串
     * @return
     */
    HTTP_CODE parse_request_line(char *text);
    /**
     * @brief * @brief 主状态机解析报文中的请求头数据
     * @param text 要解析的字符串
     * @return
     */
    HTTP_CODE parse_headers(char *text);
    /**
     * @brief * @brief 主状态机解析报文中的请求内容
     * @param text
     * @return
     */
    HTTP_CODE parse_content(char *text);
    /**
     * @brief 生成相应报文
     * @return
     */
    HTTP_CODE do_request();

    /**
     * @brief 用于将指针向后偏移，指向未处理的字符，m_start_line是已经解析的字符串
     * @return 返回未解析的字符串的开头的指针位置
     */
    char *get_line()
    {
        return m_read_buf+m_start_line;
    }
    /**
     * @brief 从状态机读取一行
     * @return 返回读取结果
     */
    LINE_STATUS parse_line();

    void unmap();

    //下面的函数，根据响应报文格式，生产对应8个部分，以下函数由do_request函数调用
    bool add_response(const char *format, ...);
    bool add_content(const char *content);
    bool add_status_line(int status, const char *title);
    bool add_headers(int content_length);
    bool add_content_type();
    bool add_content_length(int content_length);
    bool add_linger();
    bool add_blank_line();



public:
    //! epoll系统调用的内核表的文件描述符
    static int m_epollfd;
    //！ 连接的用户数量 静态数据，所有对象共享
    static int m_user_count;
    //! mysql的相关连接
    MYSQL *mysql;
    //! 读为0,写为1
    int m_state;

private:
    //！ 和客户端连接的socket套接字
    int m_sockfd;
    //! 套接字地址
    sockaddr_in m_address;
    //! 存储读取的请求报文数据
    char m_read_buf[READ_BUFFER_SIZE];
    //! 读缓冲区m_read_buf中数据的最后一个字节的下一个位置
    int m_read_idx;
    //! m_read_buf读取的位置m_check_idx
    int m_checked_idx;
    //! m_read_buf中已经解析的字符串个数
    int m_start_line;

    //! 存储发出的响应报文数据
    char m_write_buf[WRITE_BUFFER_SIZE];
    //! 指示buffer中的长度
    int m_write_idx;

    //! 主状态机的状态
    CHECK_STATE m_check_state;
    //! 请求方法
    METHOD m_method;

    //以下为解析报文中对应的6个变量
    //! 请求的文件名
    char m_real_file[FILENAME_LEN];
    //! 请求的全路径
    char *m_url;
    //! http的版本
    char *m_version;
    //! 服务器主机
    char *m_host;
    //! 消息体的长度
    int m_content_length;
    //! 是否保持连接
    bool m_linger;

    //! 读取服务器上的文件地址,通过mmap映射文件之后的地址
    char *m_file_address;
    //! 文件状态
    struct stat m_file_stat;
    //! io向量机制iovec
    struct iovec m_iv[2];
    //! 数据块数
    int m_iv_count;
    //! 是否启用POST
    int cgi;
    //! 存储请求头数据
    char *m_string;
    //! 需要发送的数据长度
    int bytes_to_send;
    //! 已经发送的数据长度
    int bytes_have_send;
    //！根路径
    char *doc_root;

    map<string, string> m_users;
    //! LE或者ET模式
    int m_TRIGMode;
    //! 是否关闭日志
    int m_close_log;

    char sql_user[100];
    char sql_passwd[100];
    char sql_name[100];

};

#endif //WEBPROJECT_HTTP_CONN_H
