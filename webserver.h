//
// Created by cheng on 23-8-30.
//

#ifndef WEBPROJECT_WEBSERVER_H
#define WEBPROJECT_WEBSERVER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <cassert>
#include <sys/epoll.h>

#include "./threadpool/threadpool.h"
#include "./http/http_conn.h"

//! 最大文件描述符
const int MAX_FD = 65536;
//! 最大事件数
const int MAX_EVENT_NUMBER = 10000;
//! 最小超时单位
const int TIMESLOT = 5;

class WebServer
{
public:
    /**
     * @brief 构造函数
     */
    WebServer();
    /**
     * @brief 析构函数
     */
    ~WebServer();

    /**
     * @brief 初始化服务器的相关信息
     * @param port 端口号
     * @param user 登录数据库的用户名
     * @param passWord  用户密码
     * @param databaseName  使用的数据库名
     * @param log_write 日志的写入方式
     * @param opt_linger 是否优雅关闭
     * @param trigmode LE和RT模式
     * @param sql_num mysql连接数
     * @param 线程数
     * @param close_log 是否关闭日志
     * @param actor_model 反应模式 Proactor 还是Reactor
     */
    void init(int port, string user, string passWord, string databaseName,int log_write, int opt_linger, int trigmode,
              int sql_num, int thread_num, int close_log, int actor_model);
    /**
     * @brief 初始化线程池
     */
    void thread_pool();
    /**
     * @brief 初始化数据库链接池
     */
    void sql_pool();
    /**
     * @brief 初始化日志系统，同步还是异步
     */
    void log_write();
    /**
     * @brief 初始化http的客户端连接的socked和服务器监听Socket是LT模式还是ET模式
     */
    void trig_mode();
    /**
     * @brief 初始化listenfd监听套接字和管道和内核事件表等信息
     */
    void eventListen();
    /**
     * @brief 主线程循环处理
     */
    void eventLoop();
    /**
     * @brief 初始化http连接的相关连接，还有定时器回调函数等
     * @param connfd 这个客户端http对应的socket套接字
     * @param client_address 客户端的地址信息
     */
    void timer(int connfd ,struct sockaddr_in  client_address);
    /**
     * @brief //若有数据传输，则将定时器往后延迟3个单位,并对新的定时器在链表上的位置进行调整
     * @param timer 需要调整的定时器
     */
    void adjust_timer(util_timer *timer);
    /**
     * @brief 处理定时器的回掉函数，之后删除定时器,关闭连接
     * @param timer 要执行回调函数的定时器
     * @param sockfd 对应的http连接的socket套接字
     */
    void deal_timer(util_timer *timer, int sockfd);
    /**
     * @brief 处理客户端的连接请求，还有初始化数据
     * @return  是否连接成功
     */
    bool dealclinetdata();
    /**
     * 处理通过管道接受到的信号
     * @param timeout 是否到检查活动连接的时间
     * @param stop_server 是否停止服务器
     * @return
     */
    bool dealwithsignal(bool &timeout, bool &stop_server);
    /**
     * @brief 处理读客户端的数据线程
     * @param sockfd
     */
    void dealwithread(int sockfd);
    /**
     * @brief 处理写数据到客户端的线程
     * @param sockfd
     */
    void dealwithwrite(int sockfd);

public:
    //基础
    //！ 端口号
    int m_port;
    //! 根目录
    char *m_root;
    //! 同步还是异步写日志
    int m_log_write;
    //！ 是否关闭日志
    int m_close_log;
    //! 选择反应堆模型，默认Proacto (0) ,(1) 表示Reactor
    int m_actormodel;
    //! 管道描述符
    int m_pipefd[2];
    //! epoll内核事件表描述符
    int m_epollfd;
    //! http用户连接数组
    http_conn *users;

    //数据库相关
    //! 数据库连接池
    connection_pool *m_connPool;
    //! 登录数据库用户名
    string m_user;
    //! 登录数据库密码
    string m_passWord;
    //! 使用数据库名
    string m_databaseName;
    //! 数据库连接数量
    int m_sql_num;

    //线程相关
    //! 线程池
    threadpool<http_conn> *m_pool;
    //! 线程数量
    int m_thread_num;

    //! epoll_event相关
    epoll_event events[MAX_EVENT_NUMBER];

    //! socket监听连接端口
    int m_listenfd;
    //! 是否优雅关闭
    int m_OPT_LINGER;
    //! listenfd和connfd的模式组合，默认使用LT + LT
    int m_TRIGMode;
    //! listenfd使用的触发模式
    int m_LISTENTrigmode;
    //! connfd使用的触发模式
    int m_CONNTrigmode;

    //定时器相关
    //! 为每个http连接分配一个定时器
    client_data *users_timer;
    //封装定时器的工具类
    Utils utils;

};



#endif //WEBPROJECT_WEBSERVER_H
