//
// Created by cheng on 23-8-28.
//

#ifndef WEBPROJECT_THREADPOOL_H
#define WEBPROJECT_THREADPOOL_H

#include<list>
#include<cstdio>
#include<exception>
#include<pthread.h>
#include "../lock/locker.h"
#include "../CGImysql/sql_connection_pool.h"

template <typename T>
class threadpool
{
public:
    /**
     * @brief 构造函数
     * @param actor_model 工作模式  0，Proactor模型 1，Reactor模型
     * @param connPool  数据库连接池
     * @param thread_number  线程数量
     * @param max_request  最大请求数
     */
    threadpool(int actor_model,connection_pool *connPool,int thread_number = 8,int max_request = 10000);
    /**
     * @brief 析构函数
     */
    ~threadpool();
    /**
     * @brief 向任务队列增加一个工作任务
     * @param request 需要添加的任务
     * @param state 任务的状态
     * @return 添加任务是否成功
     */
    bool append(T *request,int state);
    /**
     * @brief 向任务队列增加一个工作任务
     * @param request 需要添加的任务
     * @return 添加任务是否成功
     */
    bool append_p(T *requert);

private:
    /**
     * @brief 工作线程运行的函数，它不断从工作队列中取出任务并执行之，传的参数是某个对象，
     * 因为静态成员没有this指针，也不能访问类内的非静态成员
     * @param arg
     * @return
     */
    static void *worker(void *arg);
    /**
     * @brief 工作线程取出任务的运行函数
     */
    void run();


private:
    //!  线程池中的线程数量
    int m_thread_number;
    //! 请求队列的最大数量
    int m_max_request;
    //! 描述线程池的数组，大小为m_thread_number;
    pthread_t *m_threads;
    //! 请求队列
    std::list<T *> m_workqueue;
    //！ 保护请求队列的互斥锁
    locker m_queuelocker;
    //! 用信号量机制来管理是否有任务需要处理
    sem m_queuestat;
    //! 数据库连接池
    connection_pool *m_connPool;
    //! 模型切换 工作模式  0，Proactor模型 1，Reactor模型
    int m_actor_model;
};


template <typename T>
threadpool<T>::threadpool(int actor_model, connection_pool *connPool, int thread_number, int max_request)
                            :m_actor_model(actor_model),m_thread_number(thread_number),m_max_request(max_request),
                            m_threads(NULL),m_connPool(connPool)
{
                                if(thread_number <= 0 || max_request <= 0)
                                {
                                    throw std::exception();
                                }
                                m_threads = new pthread_t[m_thread_number];
                                if(!m_threads){
                                    throw std::exception();
                                }
                                for(int i = 0;i < thread_number;i++)
                                {
                                    if(pthread_create(m_threads+i,NULL,worker,this) != 0)
                                    {
                                        delete[] m_threads;
                                        throw std::exception();
                                    }
                                    //设置为脱离线程
                                    if(pthread_detach(m_threads[i]))
                                    {
                                        delete[] m_threads;
                                        throw std::exception();
                                    }
                                }
}

template <typename T>
threadpool<T>::~threadpool()
{
    delete[] m_threads;
}

template <typename T>
bool threadpool<T>::append(T *request, int state)
{
    m_queuelocker.lock();
    if(m_workqueue.size() >= m_max_request)
    {
        m_queuelocker.unlock();
        return false;
    }
    request->m_state = state;
    m_workqueue.push_back(request);
    m_queuelocker.unlock();
    m_queuestat.post();
    return true;
}

template <typename T>
bool threadpool<T>::append_p(T *request)
{
    //加锁是为了防止工作线程现在同时在队列中取任务
    m_queuelocker.lock();
    if(m_workqueue.size() >= m_max_request)
    {
        m_queuelocker.unlock();
        return false;
    }
    m_workqueue.push_back(request);
    m_queuelocker.unlock();
    //信号量调用V操作，同步工作线程
    m_queuestat.post();
    return true;
}

template <typename T>
void *threadpool<T>::worker(void *arg)
{
    threadpool *pool = (threadpool *)arg;
    pool->run();
    return pool;
}

template <typename T>
void threadpool<T>::run()
{
    while(true)
    {
        //信号量的P操作，每个线程都争抢，如果主线程post一次，只有一个线程能获得。信号量是使线程同步工作队列
        m_queuestat.wait();
        //加锁是防止主线程现在也调用队列往里面添加任务。
        m_queuelocker.lock();
        if(m_workqueue.empty())
        {
            m_queuelocker.unlock();
            continue;
        }
        T *request = m_workqueue.front();
        m_workqueue.pop_front();
        m_queuelocker.unlock();
        if(!request)
        {
            continue;
        }
        if (m_actor_model == 1)
        {
            if(request->m_state == 0)
            {
                if(request->read_once())
                {
                    //标志该套接子变为写状态
                    request->improv = 1;
                    connectionRAII mysqlcon(&request->mysql,m_connPool);
                    request->process();
                }
                else
                {
                    //标志该套接子变为写状态
                    request->improv = 1;
                    //删除这个timer和关闭socket连接
                    request->timer_flag = 1;
                }
            }
            else
            {
                if(request->write())
                {
                    request->improv = 1;
                }
                else
                {
                    //没有写成功，关闭socket，删除timer
                    request->improv = 1;
                    request->timer_flag = 1;
                }
            }
        }
        else
        {
            connectionRAII mysqlcon(&request->mysql,m_connPool);
            request->process();
        }
    }
}

#endif //WEBPROJECT_THREADPOOL_H
